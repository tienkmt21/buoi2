package Buoi_2;

public class Bai6 {
    public static void main(String[] args) {
        String str1 = "think";
        String str2 = "sing";
        checkString(str1);
        checkString(str2);
    }

    static void checkString(String str) {
        char arr1[] = str.toCharArray();
        int length = str.length();
        StringBuffer newstr = new StringBuffer();

        if (str.equals("") || str.length() < 4) {
            System.out.println("Co loi xay ra");
            return;
        }

        if (arr1[length-3] == 105 && arr1[length-2] == 110 && arr1[length-1] == 103) {
            arr1[length-3] = 'l';
            arr1[length-2] = 'y';
            for (int i = 0; i < length-1; i++) {
                newstr.append(arr1[i]);
            }
        } else {
            for (int i = 0; i < length; i++) {
                newstr.append(arr1[i]);
            }
            newstr.append("ing");
        }
        System.out.println(newstr);
    }
}
