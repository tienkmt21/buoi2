package Buoi_2;

import java.util.HashMap;

public class Bai2 {
    public static void main(String[] args) {
        String str = "HocJavaBackEnd";
        HashMap<Character,Integer> arr = new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            if (arr.containsKey(str.charAt(i))) { // nếu tồn tại +1
                arr.put(str.charAt(i),arr.get(str.charAt(i))+1);
            } else { // chưa tồn tại add vào Map
                arr.put(str.charAt(i), 1);
            }
        }
        System.out.println(arr);
    }
}
