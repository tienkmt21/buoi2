package Buoi_2;

import java.util.Scanner;

public class Bai13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input;
        System.out.println("Nhap chuoi ");
        input = sc.nextLine();
        System.out.println("Chu thuong " + input.toLowerCase());
        System.out.println("Chu hoa " + input.toUpperCase());
    }
}
