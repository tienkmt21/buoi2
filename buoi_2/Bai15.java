package Buoi_2;

public class Bai15 {
    public static void main(String[] args) {
        System.out.println(addTag("a","thẻ a"));
        System.out.println(addTag("p","thẻ p"));
        System.out.println(addTag("b","thẻ b"));
        System.out.println(addTag("c","thẻ c"));
        System.out.println(addTag("d","thẻ d"));
        System.out.println(addTag("div","thẻ div"));
    }

    public static String addTag(String tag, String text) {
        String str = "";
        if (tag.equals("i") || tag.equals("b") || tag.equals("u") || tag.equals("h1") || tag.equals("h2") || tag.equals("h3") || tag.equals("h4") || tag.equals("h5") || tag.equals("a") || tag.equals("p") || tag.equals("div")) {
            str = String.format("<%s>%s</%s>", tag, text, tag);
        } else {
            str = tag + " not match html tag";
        }
        return str;
    }
}
